# Matilib Exercise

This is a programming exercise to implement a linear algebra library to perform matrix and vector operations.

The implementation is rudementry and only enough to produce the required output for the example main function.

For this to be suitable for use in hard real time conditions, more work would have to be done. Areas of concern are the factorial implementation of the inverse, and the number of copies/allocations.

# Dependencies

* `CMake` version 3.20+
* A C++ toolchain such as clang or gcc
* `GoogleTest` for running tests - included as a submodule in `/thirdparty`

# Cloning, Building and Running

The repository contains two applications. `matlib` is the main application and `matlib-test` is a test runner.

1. Check out the reposity and submodules using the following commands
```
git clone https://gitlab.com/ragnarula2/mat-lib
cd mat-lib
git submodule update --init --recursive
```

2. Build using Cmake on linux or macos
```
mkdir build
cd build
cmake ../
make
```

3. Run tests
```
./matlib-test
```

4. Run the example application
```
./matlib
```

