#include <gtest/gtest.h>
#include "matlib.h"

TEST(Matrix, CommaInitialisation)
{
    Matrix<3, 3, int> M;

    M = 1, 2, 3,
    4, 5, 6,
    7, 8, 9;

    int value = 1;
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            ASSERT_EQ(M.get(i, j), value++);
        }
    }
}

TEST(Matrix, ExplicitInitialiser)
{
    Matrix<3, 3, int> M(0);

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            ASSERT_EQ(M.get(i, j), 0);
        }
    }
}

TEST(Matric, Get)
{
    Matrix<4, 3, int> M;

    M = 1, 2, 3,
    4, 5, 6,
    7, 8, 9,
    10, 11, 12;

    ASSERT_EQ(M.get(3, 2), 12);
}

TEST(Matrix, GetAndSetAValue)
{
    Matrix<3, 3, int> M(0);

    ASSERT_EQ(M.get(0, 0), 0);

    M.set(0, 0, 7);

    ASSERT_EQ(M.get(0, 0), 7);
}

TEST(Matrix, Equality)
{
    Matrix<3, 3, int> M(0);
    Matrix<3, 3, int> N(0);

    ASSERT_EQ(M, N);

    M.set(0, 0, 7);

    ASSERT_EQ(M == N, false);

    N.set(0, 0, 7);

    ASSERT_EQ(M, N);
}

TEST(Matrix, Transpose)
{
    Matrix<4, 3, int> A;
    Matrix<3, 4, int> B;

    A = 1, 2, 3,
    4, 5, 6,
    7, 8, 9,
    10, 11, 12;

    B = 1, 4, 7, 10,
    2, 5, 8, 11,
    3, 6, 9, 12;

    Matrix<3, 4, int> result = transpose(A);
    ASSERT_EQ(result, B);
}

TEST(Matrix, VectorMultiply)
{
    Matrix<3, 3, int> A;
    A = 1, 2, 3,
    4, 5, 6,
    7, 8, 9;

    Vector<3, int> b;
    b = 10, 11, 12;

    Vector<3, int> c;
    c = 68, 167, 266;

    const auto result = A * b;
    ASSERT_EQ(result, c);
}

TEST(Matrix, RecursiveDeterminantSolver)
{
    Matrix<3, 3, int> A;

    A = 11, 2, 3,
    4, 5, 6,
    7, 8, 91;

    const auto result = det(A);

    ASSERT_EQ(result, 3824);
}

TEST(Matrix, InverseIdentity)
{
    Matrix<3, 3, double> identity;

    identity = 1, 0, 0,
    0, 1, 0,
    0, 0, 1;

    const auto inverse_identity = inverse(identity);
    ASSERT_EQ(identity, inverse_identity);
}

TEST(Matrix, InverseTimeIdentity)
{
    Matrix<3, 3, double> identity;

    identity = 1, 0, 0,
    0, 1, 0,
    0, 0, 1;

    Matrix<3, 3, double> a;

    a = 11, 2, 3,
    4, 5, 6,
    7, 8, 91;

    const auto inverse_a = inverse(a);
    const auto a_x_inverse = a * inverse_a;
    ASSERT_TRUE(almost_equal(identity, a_x_inverse, std::numeric_limits<double>::epsilon()));
}

TEST(Matrix, MultiplyScalar)
{
    Matrix<3, 3, double> A;

    A = 1, 2, 3,
    4, 5, 6,
    7, 8, 9;

    Matrix<3, 3, double> B;

    B = 3, 6, 9,
    12, 15, 18,
    21, 24, 27;

    ASSERT_EQ(A * 3.0, B);
    ASSERT_EQ(3.0 * A, B);
}

TEST(Matrix, Main)
{
    Matrix<3, 3, double> A{};
    Vector<3, double> x;
    Vector<3, double> b;

    A = 2, 8, 5,
    1, 1, 1,
    1, 2, -1;

    b = 5, -2, 2;

    const auto inv_a = inverse(A);
    x = inv_a * b;

    Vector<3, double> result;
    result = -3, 2, -1;
    ASSERT_TRUE(almost_equal(x, result, std::numeric_limits<double>::epsilon()));
}