#pragma once

#include <cstdio>
#include <ostream>
#include <array>
#include <cmath>

// Class which represents a 'm x n' matrix.
// Must have non-zero rows and colums.
// Must have more than one element.
template <size_t num_rows, size_t num_columns, typename T>
class Matrix
{
    static_assert(num_rows > 0 && num_columns > 0, "Matrix can not have 0 rows or colums");
    static_assert(num_rows + num_columns > 1, "Matrix of one value is not supported");
    template <size_t pos>
    class Loader;

public:
    // Default constructor, leaves internal data un-initialised.
    constexpr Matrix() = default;

    // Initialise all values to init
    constexpr explicit Matrix(const T &init) : data{init}
    {
    }

    // Copy Constructor
    Matrix(const Matrix<num_rows, num_columns, T> &other)
    {
        std::copy(other.data.begin(), other.data.end(), data.begin());
    }

    // Move constructor
    Matrix(Matrix<num_rows, num_columns, T> &&other)
    {
        std::swap(data, other.data);
    }

    // Used for the comma operator initialisation
    constexpr Loader<0> operator=(const T &item)
    {
        data[0] = item;
        return Loader<0>(*this);
    }

    // "normal" assignment operator - copy and swap
    Matrix<num_rows, num_columns, T> &operator=(Matrix<num_rows, num_columns, T> other)
    {
        std::swap(data, other.data);
        return *this;
    }

    // Get a value from a particular index
    // Runtime asserts if indices are out of range
    [[nodiscard]] const T &get(const size_t row, const size_t col) const
    {
        const auto index = to_index(row, col);

        // TODO Do we want to use exceptions instead?
        assert(index < size());
        return data[index];
    }

    // Set a value at a particular index
    // Runtime asserts if a value is out of range
    void set(const size_t row, const size_t col, const T &value)
    {
        const auto index = to_index(row, col);
        assert(index < size());
        data[index] = value;
    }

    // Check if two matrices are equal (in terms of bits)
    // Use free function almost_equal when checking matrices of
    // floating point to avoid floating point error.
    [[nodiscard]] bool operator==(const Matrix &other) const
    {
        for (size_t i = 0; i < data.size(); ++i)
        {
            if (data[i] != other.data[i])
            {
                return false;
            }
        }
        return true;
    }

private:
    // Loader class used by comma operator to initialise matrix
    // Will assert at compile time if you try to put too many elemets into the matrix
    template <size_t pos>
    class Loader
    {
        Matrix &outer;

        constexpr void append(const T &item)
        {
            // First index is already initialised by operator=
            constexpr size_t index = pos + 1;
            static_assert(index < (num_rows * num_columns), "Too many elements for this matrix.");
            outer.data[index] = item;
        }

    public:
        constexpr Loader(Matrix &_outer) : outer(_outer)
        {
        }
        constexpr Loader<pos + 1> operator,(const T &item)
        {
            append(item);
            return Loader<pos + 1>(outer);
        }
    };

    // The absolute number of elements in the matrix
    [[nodiscard]] constexpr size_t size() const
    {
        return num_rows * num_columns;
    }

    // Transform row and colum indices into absolute index into internal array
    [[nodiscard]] size_t to_index(const size_t row, const size_t col) const
    {
        return (num_columns * row) + col;
    }

    std::array<T, num_rows * num_columns> data;
};

// Vector is a specialisation of Matrix with one column
// Only column vector is supported
template <size_t num_rows, typename T>
class Vector : public Matrix<num_rows, 1, T>
{
public:
    using super = Matrix<num_rows, 1, T>;
    using super::operator=;
    const T &get(const size_t elem) const
    {
        return super::get(elem, 0);
    }

    void set(const size_t elem, const T &val)
    {
        super::set(elem, 0, val);
    }
};

// Recursive type to find cofactors and determinants of matrices
// General case expands along first row of matrix
// Very slow n! algorithm...
template <size_t size, typename T>
class RecursiveDeterminantSolver
{
    const Matrix<size, size, T> &Mat;

public:
    RecursiveDeterminantSolver(const Matrix<size, size, T> &mat) : Mat(mat) {}
    T cofactor(const size_t row, const size_t col) const
    {
        // TODO Could throw instead
        assert(row < size && col < size);

        // TODO possible overflow - really only important if sum is odd or even
        const T place = std::pow(-1, row + col);
        constexpr size_t subsize = size - 1;

        Matrix<subsize, subsize, T> submat;
        for (size_t i = 0, isub = 0; i < size && isub < subsize; i++, isub++)
        {
            if (i == row)
            {
                ++i;
            }

            for (size_t j = 0, jsub = 0; j < size && jsub < subsize; j++, jsub++)
            {
                if (j == col)
                {
                    ++j;
                }

                submat.set(isub, jsub, Mat.get(i, j));
            }
        }

        return place * RecursiveDeterminantSolver<subsize, T>(submat).det();
    }

    T det() const
    {
        int sum = 0;
        for (size_t col = 0; col < size; ++col)
        {
            // expand along top row
            const T element = Mat.get(0, col);
            sum += element * cofactor(0, col);
        }

        return sum;
    }
};

// Base case for the recursive type when size is 2
template <typename T>
class RecursiveDeterminantSolver<2, T>
{
    const Matrix<2, 2, T> &Mat;

public:
    RecursiveDeterminantSolver(const Matrix<2, 2, T> &mat) : Mat(mat) {}
    T det() const
    {
        const T detminant = Mat.get(0, 0) * Mat.get(1, 1) - Mat.get(0, 1) * Mat.get(1, 0);
        return detminant;
    }
};

// Calculate the determinant of a matrix
// Matrix must be square
template <size_t num_rows, size_t num_cols, typename T>
T det(const Matrix<num_rows, num_cols, T> &arg)
{
    static_assert(num_rows == num_cols, "Determinant not implemented for non-square matrices.");
    constexpr size_t size = num_rows;
    return RecursiveDeterminantSolver<size, T>(arg).det();
}

// Invert a matrix
// Only implemented for square matrices
template <size_t num_rows, size_t num_cols, typename T>
Matrix<num_rows, num_cols, T> inverse(const Matrix<num_rows, num_cols, T> &arg)
{
    static_assert(num_rows == num_cols, "Inverse not implemented for non-square matrices.");

    constexpr size_t size = num_rows;
    // TODO optimise for fast method for 2 x 2 Matrix
    // using 'if constexpr (size == 2)'

    const auto transposed = transpose(arg);
    const RecursiveDeterminantSolver<size, T> det_solver(transposed);
    const auto determinant = det_solver.det();

    if (determinant == 0)
    {
        return {};
    }

    // TODO adjoint could be a free function
    auto adjoint = transposed;

    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size; ++j)
        {
            adjoint.set(i, j, det_solver.cofactor(i, j));
        }
    }

    return adjoint * (1 / determinant);
}

// Transpose a matrix
template <size_t num_rows, size_t num_columns, typename T>
Matrix<num_columns, num_rows, T> transpose(const Matrix<num_rows, num_columns, T> &arg)
{
    // TODO could this be done by changing how the internal array is indexed rather than
    // copying the data?
    Matrix<num_columns, num_rows, T> result;
    for (int i = 0; i < num_rows; ++i)
    {
        for (int j = 0; j < num_columns; ++j)
        {
            result.set(j, i, arg.get(i, j));
        }
    }
    return result;
}

// Multiply a matrix with a vector
template <size_t num_rows, size_t num_columns, typename T>
Vector<num_rows, T> operator*(const Matrix<num_rows, num_columns, T> &lhs,
                              const Vector<num_columns, T> &rhs)
{
    Vector<num_rows, T> result;

    for (int i = 0; i < num_rows; ++i)
    {
        T sum{};
        for (int j = 0; j < num_columns; ++j)
        {
            sum += lhs.get(i, j) * rhs.get(j);
        }
        result.set(i, sum);
    }
    return result;
}

// Matrix matrix multiplication
template <size_t lhs_rows, size_t lhs_cols_rhs_rows, size_t rhs_cols, typename T>
Matrix<lhs_rows, rhs_cols, T> operator*(const Matrix<lhs_rows, lhs_cols_rhs_rows, T> &lhs,
                                        const Matrix<lhs_cols_rhs_rows, rhs_cols, T> &rhs)
{
    Matrix<lhs_rows, rhs_cols, T> result;

    for (size_t i = 0; i < lhs_rows; ++i)
    {
        for (size_t j = 0; j < rhs_cols; ++j)
        {
            T sum = {};
            for (size_t k = 0; k < lhs_cols_rhs_rows; ++k)
            {
                sum += lhs.get(i, k) * rhs.get(k, j);
            }

            result.set(i, j, sum);
        }
    }

    return result;
}

// Matrix scalar mutiplication
template <size_t num_rows, size_t num_columns, typename T>
Matrix<num_rows, num_columns, T> operator*(const Matrix<num_rows, num_columns, T> &lhs, const T &rhs)
{
    Matrix<num_rows, num_columns, T> result;

    for (int i = 0; i < num_rows; ++i)
    {
        for (int j = 0; j < num_columns; ++j)
        {
            result.set(i, j, lhs.get(i, j) * rhs);
        }
    }
    return result;
}

// Matrix scalar multiplication
template <size_t num_rows, size_t num_columns, typename T>
Matrix<num_rows, num_columns, T> operator*(const T &lhs, const Matrix<num_rows, num_columns, T> &rhs)
{
    return rhs * lhs;
}

// Check for equality taking into account floating point error
// Use this is you want to check the results from different calculations are equal
template <size_t num_rows, size_t num_columns, typename T>
bool almost_equal(const Matrix<num_rows, num_columns, T> &lhs,
                  const Matrix<num_rows, num_columns, T> &rhs, const T &epsilon)
{
    for (int i = 0; i < num_rows; ++i)
    {
        for (int j = 0; j < num_columns; ++j)
        {
            if (std::abs(lhs.get(i, j) - rhs.get(i, j)) > epsilon)
            {
                return false;
            }
        }
    }

    return true;
}

// Print the contents of a matrix
template <size_t num_rows, size_t num_columns, typename T>
std::ostream &operator<<(std::ostream &lhs, const Matrix<num_rows, num_columns, T> &rhs)
{
    for (int i = 0; i < num_rows; ++i)
    {
        for (int j = 0; j < num_columns; ++j)
        {
            lhs << rhs.get(i, j);

            if (j < num_columns - 1)
            {
                lhs << ", ";
            }
        }
        lhs << "\n";
    }
    return lhs;
}
